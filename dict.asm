%include "lib.inc"

global find_word

section .text

;find_word принимает два аргумента:
;   • Указатель на нуль-терминированную строку (rdi)
;   • Указатель на начало словаря (rsi)
;find_word пройдёт по всему словарю в
;поисках подходящего ключа. Если подходящее вхождение найдено, вернёт адрес начала
;вхождения в словарь (не значения), иначе вернёт 0.

find_word:
.loop:
    xor rax, rax
    add rsi, 8
    push rsi
    push rdi
    call string_equals
    pop rdi
    pop rsi
    cmp rax, 1
    je .success
    mov rsi, [rsi-8]
    cmp rsi, 0
    je .fail
    jmp .loop
.success:
    mov rax, rsi
    ret
.fail:
    xor rax, rax
    ret
