%define buf_size 256

%include "lib.inc"

extern find_word
global _start

section .data
    %include "words.inc"
    error_key: db "Where is no key equal to this", 0
    error_buf_size: db "Buffer overflow", 0
    buf: times buf_size db 0

section .text
;функция _start:
;   • Читает строку размером не более 255 символов в буфер с stdin.
;   • Пытается найти вхождение в словаре; если оно найдено, распечатывает
;     в stdout значение по этому ключу. Иначе выдает сообщение об ошибке.
_start:
    sub rsp, buf_size
    mov rdi, rsp
	mov rsi, buf_size
	call read_word
	cmp rax, 0
	je .overflow
	mov rsi, last_pointer
	mov rdi, rax
	call find_word
	cmp rax, 0
	je .fail
	jmp .success

.overflow:
    mov rdi, error_buf_size
    call print_string
    call print_newline
    mov rdi, 1
    jmp .end

.fail:
    mov rdi, error_key
    call print_string
    call print_newline
    mov rdi, 1
    jmp .end

.success:
    mov rdi, rax
    call print_string
    call print_newline
    add rsp, buf_size
    xor rdi, rdi

.end:
    jmp exit

