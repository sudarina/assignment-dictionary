NASMFLAGS=-f elf64
all: main

main: main.o lib.o dict.o
	ld $^ -o $@

%.o: %.asm
	nasm $(NASMFLAGS) $^ -o $@

clean:
	rm *.o

.PHONY: clean