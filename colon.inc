%define last_pointer 0

%macro colon 2
    %ifid %2
        %ifstr %1
            %%some_label: dq last_pointer
            db %1, 0
            %2:
            %define last_pointer %%some_label
        %else
            %fatal "Key must be a string"
        %endif
    %else
        %fatal "this name can't be used for a label"
    %endif
%endmacro
